class Calculator {
    constructor(previousOpTextElement, currentOpTextElement) {
        this.previousOpTextElement = previousOpTextElement;
        this.currentOpTextElement = currentOpTextElement;
    }

    clear() {
        this.previousOp = '';
        this.currentOp = '';
        this.op = undefined;
    }

    del() {
        this.currentOp = this.currentOp.toString().slice(0, -1);

    }

    appendNum(num) {
        if (this.currentOp === undefined) {
            this.currentOp = '';
        }
        if (num === '.' && this.currentOp.includes('.')) {
            return;
        }
       
        this.currentOp = this.currentOp.toString() + num.toString();
    }

    chooseOp(op) {
        if (this.currentOp === '') {
            return;
        }
        if (this.previousOp !== '') {
            this.compute();
        } 
        this.op = op;
        this.previousOp = this.currentOp;
        this.currentOp = '';

    }

    compute() {
        let computation;
        const prev = parseFloat(this.previousOp);
        const curr = parseFloat(this.currentOp);
        if(isNaN(prev) || isNaN(curr)) {
            return;
        }
        switch(this.op) {
            case '+':
                computation = prev + curr;
                break;
                
            case '-':
                computation = prev - curr;
                break;
                
            case 'x':
                computation = prev * curr;
                break;
                
            case '÷':
                computation = prev / curr;
                break;
                
            default:
                return;
        }
    this.currentOp = computation;
    this.op = undefined;
    this.previousOp = '';
    }

    updateDisplay() {
        this.currentOpTextElement.textContent = 
            this.currentOp;
        if (this.op != null) {
            this.previousOpTextElement.textContent =
            this.previousOp + ' ' + this.op;
            return;
        }
        this.previousOpTextElement.textContent = this.currentOp;
    }

}


const numBtn = document.querySelectorAll('[data-num]')
const opBtn = document.querySelectorAll('[data-op]');
const acBtn = document.querySelector('[data-ac]');
const delBtn = document.querySelector('[data-del]');
const equalsBtn = document.querySelector('[data-equals]');
const previousOpTextElement = document.querySelector('[data-previous-op]');
const currentOpTextElement = document.querySelector('[data-current-op]');
const calculator = new Calculator(previousOpTextElement, currentOpTextElement);

numBtn.forEach(btn => {
    btn.addEventListener('click', () => {
        calculator.appendNum(btn.textContent);
        calculator.updateDisplay();
    })
}); 

opBtn.forEach(btn => {
    btn.addEventListener('click', () => {
        calculator.chooseOp(btn.textContent);
        calculator.updateDisplay();
    })
});

equalsBtn.addEventListener('click', () => {
    calculator.compute();
    calculator.updateDisplay();
})

acBtn.addEventListener('click', () => {
    calculator.clear();
    calculator.updateDisplay();
})
delBtn.addEventListener('click', () => {
    calculator.del();
    calculator.updateDisplay();
})